# random-links

Just another place to put my random links and others may care to see.



## BSD

[NextCloud on OpenBSD](https://h3artbl33d.nl/2020-nextcloud.html)

[Signal on OpenBSD](https://h3artbl33d.nl/2019-signal.html)


## Docker

[Docker 101 - Running Your Applications](https://docs.linuxserver.io/general/containers-101)

[Understanding Containerization By Recreating Docker](https://itnext.io/linux-container-from-scratch-339c3ba0411d)